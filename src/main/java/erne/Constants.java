package erne;

public class Constants {

	public static final boolean autoSpeciationThreshold = true;
	public static final int targetNSpecies = 20;
	public static final double speciationThresholdMod = 0.1;
	public static final double speciationThresholdMin = 0.03;
	public static final double probMutationOnly = 0.5;
	public static final int tournamentSize = 5;
	public static final double defaultSpeciationThreshold = 0.4;
    public static int maxEvalTime = 4000; //in steps
	public static int maxEvalClockTime = 30; //in seconds; -1 means no time out
	public static double comparisonThreshold = 0.0; //for multiobjective and lexicographic fitnesses.
	public static boolean debug = false;
	public static boolean stabilityCheck = false;
}
